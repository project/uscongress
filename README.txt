
Application Module that imports and manages Bills from the U.S. Library of
Congress website as CCK nodes.  It imports Representatives, Committees, Bills,
and Bill Actions.  It creates these CCK types when the module is installed,
and imports this data on cron runs.  It manages the automatic updating of this
data as it changes.  The administrator can control which user is the owner
of the Bill nodes, what type of bills to download, and how much time to  
spend during each cron run.

The module was built with Organic Groups in mind and works, as long as you
apply patch #185376.

The data is retrieved from Thomas via two methods -- neither is pretty, but
both are abstracted from the user.  The primary method is screenscraping.
The secondary method is by reading a document archive of XML files.  The
reason XML is the secondary method is that not all information is available
via XML.  However, the second method is important in that it is the main
indicator that something has changed.

Import methods are somewhat extensible in that they exist in the import
directory and are named uscongress_..._import().

When the Library of Congress implements a real XML import, this can be
easily updated.

Author
------
Doug Green
douggreen@douggreenconsulting.com

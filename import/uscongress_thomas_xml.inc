<?php

define('BASE_THOMAS_URL', 'http://thomas.loc.gov/home/gpoxmlc');

function uscongress_thomas_xml_import($congress, $time = 10) {
  $start = microtime(TRUE);

  // create the SQL JOIN clause to restrict the 
  $field_usc_billno = content_database_info(content_fields('field_usc_billno', 'usc_bill'));
  $field_usc_billno_table = $field_usc_billno['table'];
  $field_usc_billno_column = $field_usc_billno['columns']['value']['column'];
  $field_usc_billtype = content_database_info(content_fields('field_usc_billtype', 'usc_bill'));
  $field_usc_billtype_table = $field_usc_billtype['table'];
  $field_usc_billtype_column = $field_usc_billtype['columns']['value']['column'];
  $field_usc_congress = content_database_info(content_fields('field_usc_congressno', 'usc_bill'));
  $field_usc_congress_table = $field_usc_congress['table'];
  $field_usc_congress_column = $field_usc_congress['columns']['value']['column'];
  $join = array("INNER JOIN {". $field_usc_billno_table ."} b ON b.$field_usc_billno_column = d.billno");
  if ($field_usc_billno_table == $field_usc_billtype_table) {
    $join[0] .= " AND b.$field_usc_billtype_column = 'HR'";
  }
  else {
    $join[] .= "INNER JOIN {". $field_usc_billtype_table ."} c ON b.nid = c.nid AND b.vid = c.vid AND c.$field_usc_billtype_column = 'HR'";
  }
  if ($field_usc_billno_table == $field_usc_congress_table) {
    $join[0] .= " AND b.$field_usc_congress_column = d.congress";
  }
  else {
    $join[] .= "INNER JOIN {". $field_usc_congress_table ."} c ON b.nid = c.nid AND b.vid = c.vid AND c.$field_usc_congress_column = d.congress";
  }
  $joininfo = implode(' ', $join);

  for ($attempt = 1; $attempt; $attempt = 0) {
    $count = 0;
    $results = db_query("SELECT d.*, b.nid FROM {uscongress_documents} d ". $joininfo ." WHERE d.congress = %d AND d.modified > d.processed ORDER BY d.modified DESC", $congress);
    while ($doc = db_fetch_object($results)) {
      _uscongress_xml_retrieve_document($doc);
       if ($time && microtime(TRUE) - $start > $time) {
        break;
      }
    }
    if ($count) {
      return 1;
    }
    $result = db_query_range("SELECT name FROM {uscongress_documents} WHERE congress = %d AND modified > processed", $congress, 0, 1);
    if (db_num_rows($result) == 0) {
      _uscongress_xml_retrieve_document_list();
    }
  }
  return 0;
}

function _uscongress_xml_drupal_http_code($result) {
  return isset($result->redirect_code) ? $result->redirect_code : $result->code;
}

function _uscongress_xml_retrieve_document($doc) {
  // create the filepath to the saved document
  $path = variable_get('uscongress_files', '');
  if (!$path) {
    $path = file_directory_path();
  }
  $filepath = file_create_path($path) .'/'. $doc->name;

  // only download the file if it has changed
  $mtime = file_exists($filepath) ? filemtime($filepath) : 0;
  if ($mtime != $doc->modified) {
    // download the XML document using HTTP
    $url = BASE_THOMAS_URL . $doc->congress . '/'. $doc->name;
    $http_result = drupal_http_request($url);
    $code = _uscongress_xml_drupal_http_code($http_result);
    if ($code == 200) {
      $xml_string = $http_result->data;

      // save a copy of the XM
      file_save_data($xml_string, $filepath, FILE_EXISTS_REPLACE);
      chmod($filepath, 0644);
      touch($filepath, $doc->modified);
    }
  }
  else {
    // read the file from disk
    if ($fp = fopen($filepath, "r")) {
      $xml_string = fread($fp, filesize($filepath));
      fclose($fp);
    }
  }

  if ($xml_string) {
    // parse the xml document and save the information with the bill nodes
    $xml = simplexml_load_string($xml_string);
    if (_uscongress_xml_parse_document($doc, $xml)) {
      db_query("UPDATE {uscongress_documents} SET processed = modified WHERE congress = %d AND name = '%s'", $doc->congress, $doc->name);
    }
  }
  else {
    watchdog('error', t('error %code downloading @url', array('%code' => $code, '@url' => $url)));
  }
}

function _uscongress_xml_retrieve_document_list() {
  // download the HTML that contains all of the available documents and modified times
  $congress = variable_get('uscongress_congress', '110');
  $url = BASE_THOMAS_URL . $congress .'/';
  $http_result = drupal_http_request($url);
  $code = _uscongress_xml_drupal_http_code($http_result);
  if ($code == 200) {
    // split this HTML page into sections based on the query linking to the document
    if (preg_match_all(',<a href="([^"]*)">[^<]*</a>\s*([^ ]+)\s*([^ ]+),i', $http_result->data, $documents)) {
      // retrieve the document information stored in the database
      $results = db_query('SELECT name, modified FROM {uscongress_documents} WHERE congress = %d', $congress);
      $all_modified = array();
      while ($doc = db_fetch_object($results)) {
        $all_modified[$doc->name] = $doc->modified;
      }
    
      // update the database with the newly retrieved document information
      foreach ($documents[1] as $index => $name) {
        if (preg_match('/h([0-9]+)/i', $name, $match)) {
          $billno = $match[1];
          $document_modified = strtotime($documents[2][$index] .' '. $documents[3][$index]);
          if (isset($all_modified[$name])) {
            if ($all_modified[$name] != $document_modified) {
              db_query("UPDATE {uscongress_documents} SET modified = %d WHERE congress = %d AND name = '%s'", $document_modified, $congress, $name);
            }
          }
          else {
            db_query("INSERT INTO {uscongress_documents} (congress, billno, name, modified) VALUES (%d, %d, '%s', %d)", $congress, $billno, $name, $document_modified);
          }
        }
      }
    }
  }
  else {
    watchdog('error', t('error %code downloading @url', array('%code' => $http_result->code, '@url' => $url)));
  }
}

function _uscongress_xml_parse_document($doc, $xml) {
  if ($node = node_load($doc->nid)) {
    $node->revision = TRUE;
    $node->log = t('Updated by uscongress.module [xml] on '. date('g:i:s a'));

    $attr = $xml->attributes();
    if ($billstage = (string) $attr['bill-stage']) {
      $node->field_usc_billstage[0]['value'] = $billstage;
    }
    if ($billtype = (string) $attr['bill-type']) {
      $node->field_usc_billstyle[0]['value'] = $billtype;
    }
    if ($dmsid = (string) $attr['dms-id']) {
      $node->field_usc_dmsid[0]['value'] = $dmsid;
    }
    if ($legtype = (string) $xml->form->{'legis-type'}) {
      $node->field_usc_legtype[0]['value'] = $legtype;
    }
    if ($public = (string) $attr['public-private']) {
      $node->field_usc_public[0]['value'] = ($public == 'PRIVATE') ? 0 : 1;
    }

    // @TODO: if the sponsor/cosponsor/committee changes add the new values
    if ($action = $xml->form->action) {
      if ($action_desc = $action->{'action-desc'}) {
        if ($sponsor = $action_desc->sponsor) {
        }
        if ($cosponsor = $action_desc->cosponsor) {
        }
        if ($cosponsor = $action_desc->{'committee-name'}) {
        }
      }
    }

    if ($session = (string) $xml->form->session) {
      $sessions = array(
        'first' => 1, 'second' => 2, 'third' => 3, 'fourth' => 4,
        '1st' => 1, '2nd' => 2, '3rd' => 3, '4th' => 4
      );
      foreach ($sessions as $regex => $index) {
        if (preg_match('/'. $regex .'/i', $session)) {
          $session_num = 1;
          break;
        }
      }
      if (isset($session_num)) {
        $node->field_usc_session[0]['value'] = $session_num;
      }
    }

    if ($title = (string) $xml->form->{'official-title'}) {
      // maintain the HR#: before the title
      if (isset($node->field_usc_billtype[0]['value']) && isset($node->field_usc_billno[0]['value'])) {
        $title = _uscongress_thomas_get_fulltype($node->field_usc_billtype[0]['value']) . $node->field_usc_billno[0]['value'] .': '. $title;
      }
      $node->title = $title;
    }

    $source = BASE_THOMAS_URL . $doc->congress . '/'. $doc->name;
    if (!isset($node->field_usc_sourceurl)) {
      $node->field_usc_sourceurl = array();
    }
    foreach ($node->field_usc_sourceurl as $field_usc_sourceurl) {
      if ($field_usc_sourceurl['value'] == $source) {
        unset($source);
        break;
      }
    }
    if (isset($source)) {
      $node->field_usc_sourceurl[] = array('value' => $source);
    }

    node_save($node);

    // update with the screenscrape method in case anything else has changed
    uscongress_thomas_screenscrape_update($node);

    return 1;
  }
}

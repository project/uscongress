<?php

function uscongress_thomas_screenscrape_import($congress, $time = 10) {
  // return success if any bills are retrieved
  $success = 0;

  // keep track of the start time so that we can stop after the user-requested time interval
  $start = microtime(TRUE);

  // loop through the different bill types, getting the summary page for each
  $errortypes = array();
  while ($billtype = _uscongress_thomas_get_next_billtype()) {
    // if we've recently gotten an error for this billtype, don't try it again
    if (isset($errortypes[$billtype])) {
      // check to guarantee against an infinite loop
      if (count($errortypes) == count(_uscongress_thomas_billtypes())) {
        break;
      }
    }
    else {
      // retrieve the summary page for the billtype
      $billno = variable_get('uscongress_last_'. $billtype, 0);
      $url = 'http://thomas.loc.gov/cgi-bin/bdquery/d?d'. $congress .':'. $billno .':./list/bss/d'. $congress . $billtype .'.lst:[[o]]';
      $result = drupal_http_request($url);
      if ($result->redirect_code == 200 || $result->code == 200) {
        // parse the summary page (and get additional pages as needed), stopping when the timer expires
        switch (_uscongress_thomas_parse_listing($result->data, $congress, $billtype, $time, $start)) {
          case -1: // processed at least one successfully, but timer expired
            return 1;
          case 1:
            $success = 1;
            break;
        }
      }
      else {
        // remember that this billtype returned an error
        $errortypes[$billtype] = $billtype;
      }
    }
  }
  return $success;
}

/**
 * get the page from Thomas again and refresh the information in our database
 */
function uscongress_thomas_screenscrape_update($node) {
  // @TODO:
}

function _uscongress_thomas_billtypes() {
  static $billtypes;
  if (!isset($billtypes)) {
    $billtypes = explode(" ", variable_get('uscongress_billtypes', 'HR'));
  }
  return $billtypes;
}

function _uscongress_thomas_get_next_billtype() {
  // initialize the next billindex
  static $index;
  if (!isset($index)) {
    $index = variable_get('uscongress_next_billindex', 0);
  }

  // return one of these types
  $billtypes = _uscongress_thomas_billtypes();

  // this is the current type
  $billtype = $billtypes[$index];

  // this is the next type
  $index = ($index + 1) % count($billtypes);
  variable_set('uscongress_next_billindex', $index);

  // return the current type
  return $billtype;
}

function _uscongress_thomas_parse_listing($html, $congress, $billtype, $time, $start) {
  // split the page into sections based on the query linking to the bill
  $regex = ',<a href="(/cgi-bin/bdquery/D\?d\d+:(\d+):\./list/bss/d\d+H[A-Z]\.\w+::)">H\.[^0-9]*\d+\s*</A>:\s*,i';
  if ($bills = preg_split($regex, $html, -1, PREG_SPLIT_DELIM_CAPTURE)) {
    // skip the stuff at the top
    array_shift($bills);

    // loop through all of the bills and parse the summaries one at a time
    while (count($bills)) {
      // save the basic information
      // NOTE: the preg_split above breaks the html into an triplets
      // @TODO: get the session from somewhere
      $bill = array_splice($bills, 0, 3);
      $info = array(
        'congressno' => $congress,
        'billtype' => $billtype,
        'source' => 'http://thomas.loc.gov/'. $bill[0],
        'number' => $bill[1],
      );

      // strip the next bill number from this string, which is getting appended because the above regex isn't perfect
      $billhtml = preg_replace('/(^:\s*|<hr>.*$)/i', '', str_replace("\n", " ", $bill[2]));

      // save the summary information
      $info['listing'] = _uscongress_thomas_parse_listing_summary('<b>Title:</b>'. $billhtml);

      // retrieve the "All" page from Thomas and parse it
      $url = $info['source'] .'@@@L';
      $result = drupal_http_request($url);
      if ($result->code == 200) {
        $info['all'] = _uscongress_thomas_parse_all(str_replace("\n", " ", $result->data));
      }

      // retrieve the "Subject" page from Thomas and parse it
      $url = $info['source'] .'@@@J';
      $result = drupal_http_request($url);
      if ($result->code == 200) {
        $info['subject'] = _uscongress_thomas_parse_subject(str_replace("\n", " ", $result->data));
      }

      // save the bill
      _uscongress_thomas_add_bill_node($info);

      // move onto the next bill, only retrieve this one again during updates
      variable_set('uscongress_last_'. $billtype, $info['number']);

      // stop if we've run out of time
      if (microtime(TRUE) - $start >= $time) {
        return -1;
      }
    }
    return 1;
  }
}

function _uscongress_thomas_parse_listing_summary($html) {
  $details = explode('<br>', $html);
  foreach ($details as $detail) {
    if (preg_match(',<b>\s*(.*):\s*</b>\s*(.*)\s*,', $detail, $match)) {
      $info[strtolower($match[1])] = trim(strip_tags($match[2]));
    }
  }

  if (isset($info['sponsor'])) {
    $sponsor = $info['sponsor'];

    if ($info_sponsor = _uscongress_parse_sponsor($sponsor)) {
      $info['sponsor'] = $info_sponsor;
    }
    else {
      $info['sponsor'] = array('title' => $sponsor);
    }

    $regex = '/introduced\s+([0-9\/]+)/i';
    if (preg_match($regex, $sponsor, $match)) {
      $info['introduced'] = $match[1];
    }

    $regex = '/cosponsors\s+\(([0-9]+)\)/i';
    if (preg_match($regex, $sponsor, $match)) {
      $info['cosponsor count'] = $match[1];
    }
  }

  if (isset($info['latest major action'])) {
    $action = explode('Status:', $info['latest major action']);
    $regex = '/^([0-9\/]+)\s+(.*)/';
    if (preg_match($regex, $action[0], $match)) {
      $info['latest major action'] = $match[2];
      $info['latest major action date'] = $match[1];
    }
    else {
      $info['latest major action'] = trim($action[0]);
    }
    if (isset($action[1])) {
      $info['status'] = trim($action[1]);
    }
  }
  return $info;
}

function _uscongress_parse_sponsor($sponsor) {
  $regex = '/^(rep\s+)?([^,]+)(,\s*([^\[]+)(\s*\[([^\]]+)\])?)?/i';
  if (preg_match($regex, $sponsor, $match)) {
    return array(
      'title' => 'Rep '. $match[2] .', '. $match[4],
      'lastname' =>  $match[2],
      'firstname' => $match[4],
      'district' => $match[6],
    );
  }
}

function _uscongress_thomas_parse_all($html) {
  // split the page into sections based on the query linking to the bill
  $details = explode('<a name=', $html);
  foreach ($details as $detail) {
    if (preg_match('/^((\'([^\']+)\')|("([^"]+)"))(.*)/', $detail, $match)) {
      switch ($match[3] ? $match[3] : $match[5]) {
        case 'major actions':
          $info['major action'] = _uscongress_thomas_parse_majoractions($match[6]);
          break;
        case 'status':
          $info['action'] = _uscongress_thomas_parse_allactions($match[6]);
          break;
        case 'titles':
//        $info['title'] = _uscongress_thomas_parse_titles($match[6]);
          break;
        case 'cosponsors':
          $info['cosponsor'] = _uscongress_thomas_parse_cosponsors($match[6]);
          break;
        case 'committees':
          $info['committee'] = _uscongress_thomas_parse_committees($match[6]);
          break;
        case 'rel-bill-detail':
          $info['related'] = _uscongress_thomas_parse_relatedbills($match[6]);
          break;
        case 'amendments':
//        $info['ammendment'] = _uscongress_thomas_parse_ammendments($match[6]);
          break;
      }
    }
  }
  return $info;
}

function _uscongress_thomas_parse_subject($html) {
  if (preg_match(',CRS INDEX TERMS:\s*<ul>(.*)</ul>,', $html, $match)) {
    $rows = explode('<br>', $match[1]);
    array_shift($rows);
    foreach ($rows as $row) {
      $items[] = strip_tags($row);
    }
    return $items;
  }
}

function _uscongress_thomas_parse_majoractions($html) {
  $rows = explode('<TR>', $html);
  array_shift($rows);
  foreach ($rows as $row) {
    $cells = strip_tags($row);
    if (preg_match(',([0-9/]+)\s+(.*),', $cells, $match)) {
      $info[$match[1]] = trim($match[2]);
    }
  }
  return $info;
}

function _uscongress_thomas_parse_allactions($html) {
  $rows = explode('<dt>', $html);
  array_shift($rows);
  foreach ($rows as $row) {
    $cells = strip_tags($row);
    if (preg_match(',([0-9/]+(\s+[0-9]+:[0-9]+([ap]m)?)?)\s*:\s*(.*),', $cells, $match)) {
      $info[$match[1]] = trim($match[4]);
    }
  }
  return $info;
}

function _uscongress_thomas_parse_titles($html) {
  // @TODO:
  $rows = explode('<li>', $html);
}

function _uscongress_thomas_parse_cosponsors($html) {
  $rows = explode('<br>', $html);
  array_shift($rows);
  foreach ($rows as $row) {
    if ($info_sponsor = _uscongress_parse_sponsor(strip_tags($row))) {
      $info[] = $info_sponsor;
    }
  }
  return $info;
}

function _uscongress_thomas_parse_committees($html) {
  $rows = explode('<tr>', $html);
  array_shift($rows);
  array_shift($rows);
  foreach ($rows as $row) {
    $cells = explode('<td', $row);
    $action = trim(strip_tags('<td'. $cells[2]));
    $committee = trim(strip_tags('<td'. $cells[1]));
    $info[$committee] = $action;
  }
  return $info;
}

function _uscongress_thomas_parse_relatedbills($html) {
  $rows = explode('<tr>', $html);
  array_shift($rows);
  array_shift($rows);
  foreach ($rows as $row) {
    $cells = explode('<td', $row);
    $relationship = trim(strip_tags('<td'. $cells[2]));
    $billfullref = trim(strip_tags('<td'. $cells[1]));
    if (preg_match('/([^0-9]+)([0-9]+)/', $billfullref, $match)) {
      if ($billtype = _uscongress_thomas_get_billtype($match[1])) {
        $billtypes = _uscongress_thomas_billtypes();
        if (in_array($billtype, $billtypes)) {
          $billref = $billtype . $match[2];
          if (isset($info[$billref])) {
            $info[$billref] .= ', '. $relationship;
          }
          else {
            $info[$billref] = $relationship;
          }
        }
      }
    }
  }
  return $info;
}

function _uscongress_thomas_get_billtype($fulltype) {
  static $billtypes;
  if (!isset($billtypes)) {
    $billtypes = array(
      'H.R.' => 'HR',
      'H.RES.' => 'HE',
    );
  }
  return $billtypes[$fulltype];
}

function _uscongress_thomas_get_fulltype($billtype) {
  static $billtypes;
  if (!isset($billtypes)) {
    $billtypes = array(
      'HR' => 'H.R.',
      'HE' => 'H.RES.',
    );
  }
  return $billtypes[$billtype];
}

function _uscongress_thomas_parse_ammendments($html) {
  // @TODO:
  $rows = explode('<P>', $html);
}

function _uscongress_thomas_new_node($type, $title) {
  return (object) array(
    'type' => $type,
    'title' => $title,
    'format' => 3,
    'status' => 1,
    'promote' => 0,
    'sticky' => 0,
    'name' => variable_get('uscongress_import_author', 'admin'),
    'log' => t('Imported by uscongress.module on '. date('g:i:s a')),
  );
}

function _uscongress_thomas_new_bill_node($info) {
  // create the node
  $billref = $info['billtype'] . $info['number'];
  if ($result = db_query(_uscongress_get_query_billref_sql(), $info['congressno'], $billref)) {
    if ($node = db_fetch_object($result)) {
      // don't get a cached version
      $node = node_load($node->nid, NULL, TRUE);
      $node->log = t('Updated by uscongress.module on '. date('g:i:s a'));

      // convert the node values to form values
      _content_widget_invoke('prepare form values', $node);
      $values = (array) $node;
    }
  }
  $title = $info['number'] .': '. $info['listing']['title'];
  if (empty($node)) {
    $node = _uscongress_thomas_new_node('usc_bill', _uscongress_thomas_get_fulltype($info['billtype']) . $title);
    $values = (array) $node;
  }

  // force revisions to make sure we don't accidentally lose any data
  $node->revision = 1;

  // set the path
  $values['path'] = 'bill/'. $info['congressno'] .'/'. $billref;

  // limit the titles to 128 characters
  $values['description'] = $title;
  $values['title'] = substr($title, 0, 128);
  $values['og_description'] = substr($title, 0, 150);

  // always add this bill source
  $source = $info['source'];
  if (!isset($values['field_usc_sourceurl'])) {
    $values['field_usc_sourceurl'] = array();
  }
  foreach ($values['field_usc_sourceurl'] as $field_usc_sourceurl) {
    if ($field_usc_sourceurl['value'] == $source) {
      unset($source);
      break;
    }
  }
  if (isset($source)) {
    $values['field_usc_sourceurl'][] = array('value' => $source);
  }

  // add the basic node information
  $values['field_usc_billno'] = array(array('value' => $info['number']));
  $values['field_usc_billtype'] = array('key' => $info['billtype']);
  $values['field_usc_billref'] = array(array('value' => $billref));
  $values['field_usc_congressno'] = array('key' => $info['congressno']);

  // add the sponsor
  $nid = db_result(db_query("SELECT nid FROM {node} WHERE type = 'usc_rep' AND title = '%s'", $info['listing']['sponsor']['title']));
  if (!$nid) {
    $nid = _uscongress_thomas_add_rep_node($info['listing']['sponsor']);
  }
  if ($nid) {
    $values['field_usc_sponsor'] = array('nids' => array($nid => $nid));
  }

  // add additional summary information
  if (isset($info['session'])) {
    $values['field_usc_session'] = array('key' => $info['session']);
  }
  if (isset($info['listing']['house reports'])) {
    $values['field_usc_housereport'] = array(array('value' => $info['listing']['house reports']));
  }
  if (isset($info['listing']['senate reports'])) {
    $values['field_usc_senatereport'] = array(array('value' => $info['listing']['senate reports']));
  }

  // add the committees
  if (count($info['all']['committee'])) {
    $field_usc_committee = array();
    foreach ($info['all']['committee'] as $committee => $action) {
      $nid = db_result(db_query("SELECT nid FROM {node} WHERE type='usc_committee' AND title='%s'", $committee));
      if (!$nid) {
        $nid = _uscongress_thomas_add_committee_node($committee);
      }
      $field_usc_committee[$nid] = $nid;
    }
    if (count($field_usc_committee)) {
      $values['field_usc_committee'] = array('nids' => $field_usc_committee);
    }
  }

  // add the cosponsors
  if (count($info['all']['cosponsor'])) {
    $field_usc_cosponsor = array();
    foreach ($info['all']['cosponsor'] as $cosponsor) {
      $nid = db_result(db_query("SELECT nid FROM {node} WHERE type = 'usc_rep' AND title = '%s'", $cosponsor['title']));
      if (!$nid) {
        $nid = _uscongress_thomas_add_rep_node($cosponsor);
      }
      $field_usc_cosponsor[$nid] = $nid;
    }
    if (count($field_usc_cosponsor)) {
      $values['field_usc_cosponsor'] = array('nids' => $field_usc_cosponsor);
    }
  }

  // add the action events
  if (count($info['all']['action'])) {
    foreach ($info['all']['action'] as $date => $action) {
      $nid = _uscongress_thomas_add_action($info['congressno'], $billref, $date, $action);
    }
  }
  if (count($info['all']['major action'])) {
    foreach ($info['all']['major action'] as $date => $action) {
      $nid = _uscongress_thomas_add_action($info['congressno'], $billref, $date, $action, TRUE);
      if ($nid && strtolower($action) == 'introduced in house') {
        $actionnode = node_load($nid);
        $values['field_usc_introduced'] = array(array('node_name' => $actionnode->title ." [nid: $nid]"));
      }
    }
  }

  // add the related bills
  if (count($info['all']['related'])) {
    $values['field_usc_related_billref'] = array();
    foreach ($info['all']['related'] as $billref => $billdescription) {
      $values['field_usc_related_billref'][] = array('value' => $billref);
    }
  }

  // add the ammendments

  return array($node, $values);
}

function _uscongress_thomas_add_bill_node($info) {
  // insert/update the bill
  list($node, $form_values) = _uscongress_thomas_new_bill_node($info);
  drupal_execute('usc_bill_node_form', $form_values, $node);

  // return the node id
  if (!$node->nid) {
    $billref = $info['billtype'] . $info['number'];
    if ($result = db_query(_uscongress_get_query_billref_sql(), $info['congressno'], $billref)) {
      $node = db_fetch_object($result);
    }
  }
  return $node->nid;
}

function _uscongress_thomas_add_rep_node($person) {
  $node = _uscongress_thomas_new_node('usc_rep', $person['title']);

  foreach (array('district', 'lastname', 'firstname') as $field) {
    if (!empty($person[$field])) {
      $node_field = 'field_usc_'. $field;
      $node->$node_field = array(array('value' => $person[$field]));
    }
  }
  $node = node_submit($node);
  node_save($node);
  return $node->nid;
}

function _uscongress_thomas_add_committee_node($title) {
  $node = _uscongress_thomas_new_node('usc_committee', $title);
  $node = node_submit($node);
  node_save($node);
  return $node->nid;
}

function _uscongress_thomas_add_action($congressno, $billref, $date, $title, $major = 0) {
  // if there is an existing action, just return
  if ($result = db_query(_uscongress_thomas_get_query_billaction_sql(), $congressno, $billref, $date, $title)) {
    if ($node = db_fetch_object($result)) {
      return $node->nid;
    }
  }

  $node = _uscongress_thomas_new_node('usc_billaction', $title);
  $node->field_usc_congressno = array(array('value' => $congressno));
  $node->field_usc_billref = array(array('value' => $billref));
  $node->field_usc_actiondate = array(array('value' => $date));
  $node->field_usc_major = array(array('value' => $major));
  $node = node_submit($node);
  node_save($node);
  return $node->nid;
}

function _uscongress_thomas_get_query_billaction_sql() {
  static $sql;
  if (!isset($sql)) {
    $field_usc_congressno = content_database_info(content_fields('field_usc_congressno', 'usc_billaction'));
    $field_usc_congressno_table = $field_usc_congressno['table'];
    $field_usc_congressno_column = 'c.'. $field_usc_congressno['columns']['value']['column'];
    $sql = "SELECT n.* FROM {node_revisions} n INNER JOIN {$field_usc_congressno_table} c ON c.nid = n.nid AND c.vid = n.vid";

    $field_usc_billref = content_database_info(content_fields('field_usc_billref', 'usc_billaction'));
    $field_usc_billref_table = $field_usc_billref['table'];
    $field_usc_billref_column = $field_usc_billref['columns']['value']['column'];
    if ($field_usc_congressno_table == $field_usc_billref_table) {
      $field_usc_billref_column = "c.$field_usc_billref_column";
    }
    else {
      $sql .= " INNER JOIN {$field_usc_billref_table} b ON b.nid = c.nid AND b.vid = c.vid";
      $field_usc_billref_column = "b.$field_usc_billref_column";
    }

    $field_usc_actiondate = content_database_info(content_fields('field_usc_actiondate', 'usc_billaction'));
    $field_usc_actiondate_table = $field_usc_actiondate['table'];
    $field_usc_actiondate_column = $field_usc_actiondate['columns']['value']['column'];
    if ($field_usc_congressno_table == $field_usc_actiondate_table) {
      $field_usc_actiondate_column = "c.$field_usc_actiondate_column";
    }
    else {
      $sql .= " INNER JOIN {$field_usc_actiondate_table} d ON d.nid = c.nid AND d.vid = c.vid";
      $field_usc_actiondate_column = "d.$field_usc_actiondate_column";
    }

    $sql .= " WHERE $field_usc_congressno_column = %d AND $field_usc_billref_column = '%s' AND $field_usc_actiondate_column = '%s' AND n.title = '%s'";
  }
  return $sql;
}
